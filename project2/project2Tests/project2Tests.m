//
//  project2Tests.m
//  project2Tests
//
//  Created by Joseph Botros on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "project2Tests.h"

#import "AppDelegate.h"
#import "MainViewController.h"
#import "GoodGameplay.h"
#import "EvilGameplay.h"

@interface project2Tests ()

    @property (nonatomic, readwrite, weak) AppDelegate *appDelegate;
    @property (nonatomic, readwrite, weak) MainViewController *viewController;
    @property (nonatomic, readwrite, weak) UIView *view;

@end


@implementation project2Tests

@synthesize appDelegate=_appDelegate;
@synthesize viewController=_viewController;
@synthesize view=_view;

- (void)setUp
{
    [super setUp];
    
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    self.viewController = self.appDelegate.mainViewController;
    self.view = self.viewController.view;
}

- (void)testNewGame
{
    [self.viewController newGame:nil];
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults]; 
    int guesses = [settings integerForKey:@"guesses"];
    int letters = [settings integerForKey:@"letters"];
    BOOL evil = [settings boolForKey:@"evil"];
    NSString *class = @"";
    if (evil)
        class = @"EvilGameplay";
    else
        class = @"GoodGameplay";
        
    STAssertEquals(self.viewController.game.guesses, guesses, @"Should equal stored guesses.");
    STAssertEquals(self.viewController.game.letters, letters, @"Should equal stored letters.");
    STAssertTrue([NSStringFromClass([self.viewController.game class]) isEqualToString: class] == YES, @"Should be the proper mode.");

}

- (void)testInputChar
{
    [self.viewController newGame:nil];
    
    self.viewController.textField.text = @"a";
    [self.viewController checkMove:nil];    
    STAssertTrue(self.viewController.game.guesses != 0, @"Gameplay should be executed.");
    
    self.viewController.textField.text = @"0";
    [self.viewController checkMove:nil];    
    STAssertTrue(self.viewController.game.guesses != 0, @"Gameplay should be executed.");
    
    self.viewController.textField.text = @"-";
    [self.viewController checkMove:nil];    
    STAssertTrue(self.viewController.game.guesses != 0, @"Gameplay should be executed.");
    
    self.viewController.textField.text = @"";
    [self.viewController checkMove:nil];    
    STAssertTrue(self.viewController.game.guesses != 0, @"Gameplay should be executed.");
}

- (void)testGoodGameplay
{
    self.viewController.game = [[GoodGameplay alloc] init];
    self.viewController.game.letters = 6;
    self.viewController.game.correctWord = @"botros";
    self.viewController.game.guessedLetters = @"";
    self.viewController.game.wrongLetters = @"";
    
    [self.viewController playGame:'r'];
    [self.viewController playGame:'o'];
    [self.viewController playGame:'b'];
    [self.viewController playGame:'s'];
    [self.viewController playGame:'t'];

    STAssertTrue(self.viewController.game.won == TRUE, @"Should have won game");
}

- (void)testEvilGameplay
{
    self.viewController.game = [[EvilGameplay alloc] init];
    self.viewController.game.letters = 4;
    NSArray *array = [[NSArray alloc] initWithObjects:@"boar", @"duck", @"bear", @"deer", @"hare", nil];
    self.viewController.game.dictionary = [[NSDictionary alloc] initWithObjects:array forKeys:array];
    self.viewController.game.guessedLetters = @"";
    self.viewController.game.wrongLetters = @"";
    
    int i = 0;
    [self.viewController playGame:'r'];
    for (NSString *key in self.viewController.game.dictionary)
    {
        NSString *word = [self.viewController.game.dictionary objectForKey:key];
        if (word == @"boar" || @"duck" || @"bear" || @"deer" || @"hare")
            i++;
    }
    STAssertTrue(i == 3, @"Equivalence group is not correct");
}

@end
