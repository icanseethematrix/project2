//
//  GameplayDelegate.h
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GameplayDelegate <NSObject>

@property (copy, nonatomic, readwrite) NSString *correctWord;
@property (copy, nonatomic, readwrite) NSDictionary *dictionary;
@property (copy, nonatomic, readwrite) NSString *guessedLetters;
@property (assign, nonatomic) NSInteger guesses;
@property (assign, nonatomic) NSInteger letters;
@property (assign, nonatomic, readwrite) BOOL won;
@property (copy, nonatomic, readwrite) NSString *wrongLetters;

- (NSMutableArray *)checkWordForMove:(char)move;
- (void)newGame;

@end
