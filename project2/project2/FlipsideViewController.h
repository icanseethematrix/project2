//
//  FlipsideViewController.h
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlipsideViewController;

@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end

@interface FlipsideViewController : UIViewController

@property (weak, nonatomic) id <FlipsideViewControllerDelegate> delegate;
@property (nonatomic, weak) IBOutlet UILabel *guessesLabel;
@property (nonatomic, weak) IBOutlet UISlider *guessesSlider;
@property (nonatomic, weak) IBOutlet UILabel *lettersLabel;
@property (nonatomic, weak) IBOutlet UISlider *lettersSlider;
@property (nonatomic, weak) IBOutlet UISwitch *evilSwitch;

- (IBAction)done:(id)sender;
- (IBAction)getEvilMode:(id)sender;
- (IBAction)getNumberOfGuesses:(id)sender;
- (IBAction)getNumberOfLetters:(id)sender;
- (IBAction)showNumberOfLetters:(id)sender;
- (IBAction)showNumberOfGuesses:(id)sender;

@end