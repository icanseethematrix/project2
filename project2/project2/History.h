//
//  History.h
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface History : NSObject

@property (assign, nonatomic, readwrite) int currentLetters;
@property (copy, nonatomic, readwrite) NSNumber *currentScore;
@property (copy, nonatomic, readwrite) NSString *currentWord;
@property (nonatomic, readwrite, weak) NSDictionary *highScores;

- (void)checkHighScore;
- (NSArray *)sortScores;

@end
