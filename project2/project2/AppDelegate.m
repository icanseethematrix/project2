//
//  AppDelegate.m
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

@implementation AppDelegate

@synthesize mainViewController = _mainViewController;
@synthesize window = _window;


/*
 *  Allows for extra application setup.
 */

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    self.window.rootViewController = self.mainViewController;
    [self.window makeKeyAndVisible];

    // get words from plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"words" ofType:@"plist"];
    NSArray *words = [[NSArray alloc] initWithContentsOfFile:path];
    
    // find largest word in dictionary, store for later use
    int maxLength = 0;
    for (NSString *word in words)
    {
        if([word length] > maxLength)
            maxLength = [word length];
    }
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    [settings setInteger:maxLength forKey:@"maxLength"];
    
    [self.mainViewController newGame:nil];
    return YES;
}


@end
