//
//  History.m
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "History.h"

@implementation History

@synthesize currentLetters = _currentLetters;
@synthesize currentScore = _currentScore;
@synthesize currentWord = _currentWord;
@synthesize highScores = _highScores;


/*
 *  Checks if user's score is eligible for high score status.
 */

- (void)checkHighScore
{
    // prepare high scores dictionary for the given word length
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"highScores %d", self.currentLetters];
    NSMutableDictionary *highScoresDic = [[NSMutableDictionary alloc] initWithDictionary:_highScores];
    
    // if high scores are empty, add current score to list
    if (_highScores == nil)
        [highScoresDic setObject:self.currentScore forKey:self.currentWord];
    
    // initialize variables
    NSNumber *oldScore = [highScoresDic objectForKey:self.currentWord];
    NSNumber *worstScore = [NSNumber numberWithInt:0];
    NSString *worstScoringWord = @"";
        
    // if entry for word already present
    if (oldScore != nil)
    {
        if ([self.currentScore compare:oldScore] == NSOrderedAscending)
            [highScoresDic setObject:self.currentScore forKey:self.currentWord];
    }
    
    // if no entry for word and other high scores present, check if current score should be added 
    else
    {
        // first add current word to list
        [highScoresDic setObject:self.currentScore forKey:self.currentWord];

        // then if too many scores, delete the worst
        NSArray *scoringWords = [highScoresDic allKeys];
        if ((int)[scoringWords count] > 10)
        {
            for (NSString *scoringWord in scoringWords)
            {
                NSNumber *thisScore = [highScoresDic objectForKey:scoringWord];
                if ([thisScore compare:worstScore] == NSOrderedDescending || [thisScore compare:worstScore] ==  NSOrderedSame)
                {
                    worstScoringWord = scoringWord;
                    worstScore = thisScore;
                }                
            }
        
            [highScoresDic removeObjectForKey:worstScoringWord];
        }
    }

    // set property and store scores
    self.highScores = [NSDictionary dictionaryWithDictionary:highScoresDic];
    [settings setValue:_highScores forKey:key];
}


/*
 *  Override synthesized getter to get latest version of high scores.
 */

- (NSDictionary *)highScores
{
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"highScores %d", self.currentLetters];
    return [settings objectForKey:key];
}


/*
 *  Sorts entries of high score dictionary by score.
 */

- (NSArray *)sortScores
{
    NSComparator sorter = ^NSComparisonResult(id a, id b)
    {
        NSNumber *a1 = a;
        NSNumber *a2 = b;
        return [a1 compare:a2]; 
    };
    
    return [self.highScores keysSortedByValueUsingComparator:sorter];
}


@end
