//
//  GoodGameplay.m
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "GoodGameplay.h"

@implementation GoodGameplay

@synthesize correctWord = _correctWord;
@synthesize dictionary = _dictionary;
@synthesize guessedLetters = _guessedLetters;
@synthesize guesses = _guesses;
@synthesize letters = _letters;
@synthesize won = _won;
@synthesize wrongLetters = _wrongLetters;


/*
 *  Checks if move is in the given word.
 */

- (NSMutableArray *)checkWordForMove:(char)move
{
    // set up variables
    NSMutableArray *locations = [[NSMutableArray alloc] initWithCapacity:0];
    NSString *moveAsString = [NSString stringWithFormat:@"%c", move];
    BOOL match = FALSE;
    NSRange hangmanLetterRange;
    
    // iterate through word to look for move
    for (int i = 0; i < self.letters; i++)
    {
        hangmanLetterRange = NSMakeRange(i, 1);
        NSString *tempString = [self.correctWord substringWithRange:hangmanLetterRange];
        
        // if move found, update guessed letters and add its location to array
        if ([moveAsString caseInsensitiveCompare:tempString] == NSOrderedSame)
        {
            self.guessedLetters = [NSString stringWithFormat:@"%@%c", self.guessedLetters, move];
            NSNumber *location = [NSNumber numberWithInt:i]; 
            [locations addObject:location];
            match = TRUE;
        }        
    }
    
    // if no move found, penalize user and add to wrong letters
    if (match == FALSE)
    {
        self.guesses--;
        self.wrongLetters = [NSString stringWithFormat:@"%@%c", self.wrongLetters, move];
        self.guessedLetters = [NSString stringWithFormat:@"%@%c", self.guessedLetters, move];
    }
    
    // if all letters in the word have been guessed, you win!
    if (self.guessedLetters.length - self.wrongLetters.length == self.correctWord.length)
        self.won = TRUE;

    return locations;
}


/*
 *  Starts new game in good mode.
 */

- (void)newGame
{
    // get words from plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"words" ofType:@"plist"];
    NSArray *words = [[NSArray alloc] initWithContentsOfFile:path];

    // get words of desired length
    NSMutableArray *selectedWords = [[NSMutableArray alloc] init];    
    int numWords = 0;
    for (NSString *word in words)
    {
        if (word.length == self.letters)
        {
            [selectedWords addObject:word];
            numWords++;
        }        
    }

    // pick word at random and initialize properties (log left for your convenience!)
    int i = arc4random() % numWords;
    self.correctWord = [selectedWords objectAtIndex:i];
    NSLog(@"%@", self.correctWord);
    self.wrongLetters = @"";
    self.guessedLetters = @"";
    self.won = FALSE;
}


@end