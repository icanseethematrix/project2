//
//  FlipsideViewController.m
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <math.h>
#import "FlipsideViewController.h"

@implementation FlipsideViewController

@synthesize delegate = _delegate;
@synthesize lettersLabel = _lettersLabel;
@synthesize lettersSlider = _lettersSlider;
@synthesize guessesLabel = _guessesLabel;
@synthesize guessesSlider = _guessesSlider;
@synthesize evilSwitch = _evilSwitch;


/*
 *  Determine which orientations to support.
 */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


/*
 *  Customize view after loading.
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    NSInteger maxLength = [settings integerForKey:@"maxLength"];
    NSInteger letters = [settings integerForKey:@"letters"];
    NSInteger guesses = [settings integerForKey:@"guesses"];
    BOOL evil = [settings boolForKey:@"evil"];
    
    self.lettersLabel.text = [NSString stringWithFormat:@"%d", letters];
    self.lettersSlider.maximumValue = maxLength;
    self.lettersSlider.value = letters;
    self.guessesLabel.text = [NSString stringWithFormat:@"%d", guesses];
    self.guessesSlider.value = guesses;
    [self.evilSwitch setOn:evil animated:NO];
}


#pragma mark - Actions


/*
 *  Return to main view.
 */

- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}


/*
 *  Get user input for mode of game.
 */

- (IBAction)getEvilMode:(id)sender
{
    UISwitch *b = (UISwitch *)sender;
    BOOL evil = b.on;
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    [settings setBool:evil forKey:@"evil"];
}


/*
 *  Get user input for number of guesses in game.
 */

- (IBAction)getNumberOfGuesses:(id)sender
{
    UISlider *b = (UISlider *)sender;
    int guesses = round(b.value);
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    [settings setInteger:guesses forKey:@"guesses"];
}


/*
 *  Get user input for number of letters in game.
 */

- (IBAction)getNumberOfLetters:(id)sender
{
    UISlider *b = (UISlider *)sender;
    int letters = round(b.value);
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    [settings setInteger:letters forKey:@"letters"];
}


/*
 *  Continuously updates number of guesses displayed.
 */

- (IBAction)showNumberOfGuesses:(id)sender
{
    UISlider *b = (UISlider *)sender;
    int letters = round(b.value);
    self.guessesLabel.text = [NSString stringWithFormat:@"%d", letters];
}


/*
 *  Continuously updates number of letters displayed.
 */

- (IBAction)showNumberOfLetters:(id)sender
{
    UISlider *b = (UISlider *)sender;
    int letters = round(b.value);
    self.lettersLabel.text = [NSString stringWithFormat:@"%d", letters];
}


@end