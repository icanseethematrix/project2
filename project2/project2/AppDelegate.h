//
//  AppDelegate.h
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) MainViewController *mainViewController;

@end
