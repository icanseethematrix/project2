//
//  EvilGameplay.m
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "EvilGameplay.h"

@implementation EvilGameplay

@synthesize correctWord = _correctWord;
@synthesize dictionary = _dictionary;
@synthesize guessedLetters = _guessedLetters;
@synthesize guesses = _guesses;
@synthesize letters = _letters;
@synthesize won = _won;
@synthesize wrongLetters = _wrongLetters;


/*
 *  Find largest equivalence group and determine move's location within.
 */

- (NSMutableArray *)checkWordForMove:(char)move
{
    // initialize variables
    NSMutableArray *locations = [[NSMutableArray alloc] initWithCapacity:0];
    NSString *moveAsString = [NSString stringWithFormat:@"%c", move];
    BOOL match = FALSE;
    NSRange hangmanLetterRange;
    NSMutableDictionary *groups = [[NSMutableDictionary alloc] init];
    
    // add each word to equivalence group
    NSString *word = @"";
    for (NSString *key in self.dictionary)
    {
        word = [self.dictionary objectForKey:key];
        if (word.length == self.letters)
        {
            NSString *key = @"X";
            for (int i = 0; i < self.letters; i++)
            {
                hangmanLetterRange = NSMakeRange(i, 1);
                NSString *tempString = [word substringWithRange:hangmanLetterRange];
                
                if ([moveAsString caseInsensitiveCompare:tempString] == NSOrderedSame)
                    key = [NSString stringWithFormat:@"%@%d", key, i];
            }
            
            // if no equivalence group yet, create one
            NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
            if ([groups objectForKey:key] == nil)
                temp = [[NSMutableDictionary alloc] init];        
            else
                temp = [groups objectForKey:key];
            
            // add word to equivalence group, and group to groups
            [temp setObject:word forKey:word];
            [groups setObject:temp forKey:key];
        }
    }
    
    // find largest equivalence group
    NSMutableDictionary *big = [groups objectForKey:@"X"];
    for (NSString *key in groups)
    {
        NSMutableDictionary *temp = [groups objectForKey:key];
        if ([temp count] > [big count])
            big = temp;
    }
    
    // largest equivalence group is new word set
    self.dictionary = big;
    
    // get sample word from group and determine move's location within
    for (NSString *key in self.dictionary)
    {
        word = [self.dictionary objectForKey:key];
        NSLog(@"%@",word);
        break;
    }
    for (int i = 0; i < self.letters; i++)
    {
        hangmanLetterRange = NSMakeRange(i, 1);
        NSString *tempString = [word substringWithRange:hangmanLetterRange];
        
        if ([moveAsString caseInsensitiveCompare:tempString] == NSOrderedSame)
        {
            self.guessedLetters = [NSString stringWithFormat:@"%@%c", self.guessedLetters, move];
            NSNumber *location = [NSNumber numberWithInt:i]; 
            [locations addObject:location];
            match = TRUE;
            
        }
    }
    
    // if group does not contain move, penalize user and add to wrong letters
    if (match == FALSE)
    {
        self.guesses--;
        self.wrongLetters = [NSString stringWithFormat:@"%@%c", self.wrongLetters, move];
        self.guessedLetters = [NSString stringWithFormat:@"%@%c", self.guessedLetters, move];
    }
    
    // if you guess all letters of a single-entry equivalence group, you win!
    if (self.guessedLetters.length - self.wrongLetters.length == self.letters)
    {
        for (NSString *key in self.dictionary)
        {
            self.correctWord = [self.dictionary objectForKey:key];
            break;
        }
        self.won = TRUE;
    }
    
    return locations;    
}


/*
 *  Starts a new game in evil mode.
 */

- (void)newGame
{
    // get words in plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"words" ofType:@"plist"];
    NSArray *words = [[NSArray alloc] initWithContentsOfFile:path];
    NSMutableDictionary *selectedWords = [[NSMutableDictionary alloc] init];
    
    // get words of desired length
    int numWords = 0;
    for (NSString *word in words)
    {
        if (word.length == self.letters)
        {
            [selectedWords setObject:word forKey:[NSString stringWithFormat:@"%d",numWords]];
            numWords++;
        }
    }
    
    // initialize properties
    self.dictionary = selectedWords;
    self.wrongLetters = @"";
    self.guessedLetters = @"";
    self.won = FALSE;
}


@end