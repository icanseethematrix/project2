//
//  EvilGameplay.h
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameplayDelegate.h"

@interface EvilGameplay : NSObject <GameplayDelegate>

@property (copy, nonatomic, readwrite) NSMutableDictionary *dictionary;

@end
