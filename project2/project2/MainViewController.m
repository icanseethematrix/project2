//
//  MainViewController.m
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "EvilGameplay.h"
#import "GoodGameplay.h"
#import "History.h"
#import "MainViewController.h"

@implementation MainViewController

@synthesize evil = _evil;
@synthesize game = _game;
@synthesize guessesLabel = _guessesLabel;
@synthesize history = _history;
@synthesize lettersLabel = _lettersLabel;
@synthesize progressView = _progressView;
@synthesize textField = _textField;
@synthesize wordLabel = _wordLabel;


/*
 *  Ensure move is valid.
 */

- (IBAction)checkMove:(id)sender
{            
    char move;

    // ensure field text is not nil, then get move and reclear field
    if ([self.textField.text length] != 0) 
    {    
        move = [self.textField.text characterAtIndex:0];
        self.textField.text = @"";
    }
    
    // ensure move is an alphabetic character
    if (!isalpha(move))
        return;
    
    // if valid character, make sure it hasn't already been guessed
    else
    {
        for (int i = 0; i < self.game.guessedLetters.length; i++)
        {
            // if already guessed, alert user
            if ([self.game.guessedLetters characterAtIndex:i] == move) 
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"You already guessed that."
                                                               delegate:self 
                                                      cancelButtonTitle:@"OK!"
                                                      otherButtonTitles:nil, nil];
                [alert show];
                return;
            }
        }
    }   
    
    // if valid move, play the game!
    [self playGame:move];
}


/*
 *  Starts a new game.
 */

- (IBAction)newGame:(id)sender
{    
    // determine in what mode to play and initialize game
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    self.evil = [settings boolForKey:@"evil"];
    if (self.evil)
        self.game = [[EvilGameplay alloc] init];
    else
        self.game = [[GoodGameplay alloc] init];
    
    // set game properties and start playing!
    self.game.letters = [settings integerForKey:@"letters"];
    self.game.guesses = [settings integerForKey:@"guesses"];
    [self.game newGame];
    
    // start high scores object for the given game
    self.history = [[History alloc] init];
    self.history.currentLetters = self.game.letters;
    NSString *key = [NSString stringWithFormat:@"highScores %d", self.history.currentLetters];
    self.history.highScores = [settings objectForKey:key];
    
    // format view
    NSMutableString *text = [NSMutableString stringWithString:@""];
    for (int i = 0; i < self.game.letters; i++)
        [text appendString:@"-"];
    self.wordLabel.text = text;
    self.guessesLabel.text = [NSString stringWithFormat:@"%d", self.game.guesses];
    self.lettersLabel.text = @"";
    [self.progressView setProgress:1 animated:YES];

    // reopen keyboard
    [self.textField becomeFirstResponder];
}


/*
 *  Checks whether move is in word and acts accordingly.
 */

- (void)playGame:(char)move
{        
    // keep track of previous number of guesses
    int previousGuesses = self.game.guesses; 
    
    // check if in word and update wrong letters
    NSArray *locations = [self.game checkWordForMove:move];
    self.history.currentScore = [NSNumber numberWithInt:self.game.wrongLetters.length];
        
    // if user runs out of guesses, game over!
    if (self.game.guesses == 0)
    {            
        [self.textField resignFirstResponder];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Game Over!" 
                                                        message:@"Try again?"
                                                        delegate:nil 
                                                cancelButtonTitle:@"OK!" 
                                                otherButtonTitles:nil];
        [alert show];
        
        HistoryViewController *controller = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController" bundle:nil];
        controller.delegate = self;
        
        // ensure latest version of high scores
        controller.highScores = self.history.highScores;
        controller.sortedScores = [self.history sortScores];

        // show high scores
        controller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentModalViewController:controller animated:YES];
    }        

    // if move was in word, display it in proper locations
    else if (locations != nil)
    {
        for (NSNumber *location in locations)
        {
            int i = [location intValue];
            NSRange hangmanLetterRange = NSMakeRange(i, 1);
            self.wordLabel.text = [self.wordLabel.text stringByReplacingCharactersInRange:hangmanLetterRange withString:[NSString stringWithFormat:@"%c",move]];
        }
    
        // you win!
        if (self.game.won)
        {        
            // show correct word and update high scores
            self.wordLabel.text = [self.game.correctWord lowercaseString];
            self.history.currentWord = self.game.correctWord;
            [self.history checkHighScore];
            
            // alert user
            [self.textField resignFirstResponder];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You win!" 
                                                            message:@"Well played. Another game?" 
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK!" 
                                                  otherButtonTitles:nil];
            [alert show];
            
            HistoryViewController *controller = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController" bundle:nil];
            controller.delegate = self;
            
            // ensure latest version of high scores
            controller.highScores = self.history.highScores;            
            controller.sortedScores = [self.history sortScores];
            
            // show high scores
            controller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            [self presentModalViewController:controller animated:YES];
        }
    }           
    
    // format view
    float progress = self.progressView.progress * self.game.guesses / previousGuesses; 
    self.guessesLabel.text = [NSString stringWithFormat:@"%d", self.game.guesses];
    self.lettersLabel.text = [NSString stringWithFormat:@"%@", self.game.wrongLetters];
    [self.progressView setProgress:progress animated:YES];
}


/*
 *  Determines orientations to support.
 */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


/*
 *  Displays settings view.
 */

- (IBAction)showInfo:(id)sender
{    
    FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideViewController" bundle:nil];
    controller.delegate = self;
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:controller animated:YES];
}


/*
 *  Customize view after loading.
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // bring up keyboard and ready field for input
    self.textField.hidden = YES;
    [self.textField becomeFirstResponder];
    self.textField.text = @"";    
}


#pragma mark - Other Views


/*
 *  Dismisses settings view.
 */

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    [self dismissModalViewControllerAnimated:YES];
}


/*
 *  Dismisses high scores view.
 */

- (void)historyViewControllerDidFinish:(HistoryViewController *)controller
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
