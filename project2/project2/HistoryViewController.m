//
//  HistoryViewController.m
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "HistoryViewController.h"

@implementation HistoryViewController

@synthesize delegate = _delegate;
@synthesize highScores = _highScores;
@synthesize sortedScores = _sortedScores;
@synthesize tblSimpleTable = _tblSimpleTable;


/*
 *  Determines orientations to support.
 */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


/*
 *  Populates high score table.
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSString *key = [self.sortedScores objectAtIndex:[indexPath row]];
    NSString *rowContent = [NSString stringWithFormat:@"%@ - %@", [self.highScores objectForKey:key], key];
    
    cell.textLabel.text = rowContent;
    
    return cell;
}


/*
 *  Determines number of high scores to be displayed in table.
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.highScores count];
}


#pragma mark - Actions


/*
 *  Return to main view.
 */

- (IBAction)done:(id)sender
{
    [self.delegate historyViewControllerDidFinish:self];
}


@end