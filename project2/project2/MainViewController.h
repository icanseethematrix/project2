//
//  MainViewController.h
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "FlipsideViewController.h"
#import "GameplayDelegate.h"
#import "History.h"
#import "HistoryViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate, HistoryViewControllerDelegate>

@property (assign, nonatomic) BOOL evil;
@property (nonatomic, strong) id<GameplayDelegate> game;
@property (nonatomic, weak) IBOutlet UILabel *guessesLabel;
@property (nonatomic, strong) History *history;
@property (nonatomic, weak) IBOutlet UILabel *lettersLabel;
@property (nonatomic, weak) IBOutlet UIProgressView *progressView;
@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UILabel *wordLabel;

- (IBAction)checkMove:(id)sender;
- (IBAction)newGame:(id)sender;
- (void)playGame:(char)move;
- (IBAction)showInfo:(id)sender;


@end
