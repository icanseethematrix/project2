//
//  HistoryViewController.h
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HistoryViewController;

@protocol HistoryViewControllerDelegate
- (void)historyViewControllerDidFinish:(HistoryViewController *)controller;
@end

@interface HistoryViewController : UIViewController

@property (nonatomic, weak) id<HistoryViewControllerDelegate> delegate;
@property (nonatomic, readwrite, strong) NSDictionary *highScores;
@property (nonatomic, readwrite, strong) NSArray *sortedScores;
@property (nonatomic, readwrite, strong) IBOutlet UITableView *tblSimpleTable;
 
- (IBAction)done:(id)sender;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

@end