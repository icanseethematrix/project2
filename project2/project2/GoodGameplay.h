//
//  GoodGameplay.h
//  project2
//
//  Created by Joseph Botros/Ibrahim Sheikh on 3/22/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameplayDelegate.h"

@interface GoodGameplay : NSObject <GameplayDelegate>

@end